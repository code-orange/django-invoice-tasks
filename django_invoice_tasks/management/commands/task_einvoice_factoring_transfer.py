from django.core.management.base import BaseCommand

from django_invoice_tasks.django_invoice_tasks.tasks import einvoice_factoring_transfer


class Command(BaseCommand):
    def handle(self, *args, **options):
        einvoice_factoring_transfer()
