from django.core.management.base import BaseCommand

from django_invoice_tasks.django_invoice_tasks.tasks import einvoice_tax_transfer


class Command(BaseCommand):
    help = "Run task: einvoice_collector_month"

    def handle(self, *args, **options):
        einvoice_tax_transfer()
