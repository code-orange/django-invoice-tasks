from django.core.management.base import BaseCommand

from django_invoice_tasks.django_invoice_tasks.tasks import einvoice_send


class Command(BaseCommand):
    def handle(self, *args, **options):
        einvoice_send()
