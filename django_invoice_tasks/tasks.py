import os
import shutil
import tempfile
from base64 import b64encode, b64decode
from datetime import datetime
from io import BytesIO
from itertools import chain
from math import ceil

import slumber
import xlsxwriter
from PyPDF2 import PdfFileWriter, PdfFileReader
from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMessage
from django.template import engines
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.pdfgen import canvas

from django_cks_dms_models.django_cks_dms_models.models import CksFiles
from django_invoice_models.django_invoice_models.models import *
from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_sap_business_one_models.django_sap_business_one_models.models import (
    Oinv,
    Orin,
    CksB1Toarchive,
    CksArchive,
    Octg,
    Ocrd,
)
from django_simple_notifier.django_simple_notifier.models import (
    SnotifierTemplate,
    SnotifierEmailContactZammad,
)
from django_simple_notifier.django_simple_notifier.plugin_zammad import send


def archive_write_file(prefix: str, docnum: int, data):
    # TODO: unlink tmp folder at end of run
    temp_path = tempfile.mkdtemp()
    filename = os.path.join(
        temp_path, "DolphinIT_" + prefix + "_" + str(docnum) + ".pdf"
    )
    pdf_file = open(filename, "wb")
    pdf_file.write(data)
    pdf_file.close()

    return filename


def archive_get_attachment(prefix: str, docnum: int, data):
    attachment = dict()
    attachment["filename"] = "DolphinIT_" + prefix + "_" + str(docnum) + ".pdf"
    attachment["data"] = b64encode(data).decode("utf-8")
    attachment["mime-type"] = "application/pdf"

    return attachment


@shared_task(name="einvoice_send")
def einvoice_send():
    # fetch invoice settings
    sequence_einv_oinv = InvoiceSettings.objects.get(name="sequence_einv_oinv")
    sequence_einv_orin = InvoiceSettings.objects.get(name="sequence_einv_orin")

    # fetch documents
    oinv_unprocessed = Oinv.objects.filter(
        docentry__gt=sequence_einv_oinv.value
    ).order_by("docentry")
    orin_unprocessed = Orin.objects.filter(
        docentry__gt=sequence_einv_orin.value
    ).order_by("docentry")

    document_list = list(chain(oinv_unprocessed, orin_unprocessed))

    # process outbox
    for doc in document_list:
        django_engine = engines["django"]

        template_opts = dict()
        template_opts["cardcode"] = doc.cardcode.cardcode
        template_opts["cardname"] = doc.cardcode.cardname
        template_opts["doc_number"] = doc.docnum
        template_opts["doc_date"] = doc.docdate.strftime("%d.%m.%Y")
        template_opts["doc_due_date"] = doc.docduedate.strftime("%d.%m.%Y")
        template_opts["doc_total"] = str(round(doc.doctotal, 2))

        # Payment terms
        payment_terms = Octg.objects.get(groupnum=doc.groupnum)
        template_opts["payment_terms"] = payment_terms.pymntgroup

        if isinstance(doc, Oinv):
            doc_prefix = "Rechnung"
            doc_obj_type = 13

            email_template = SnotifierTemplate.objects.get(name="invoice").template

            if doc.docentry > int(sequence_einv_oinv.value):
                sequence_einv_oinv.value = str(doc.docentry)

        elif isinstance(doc, Orin):
            doc_prefix = "Gutschrift"
            doc_obj_type = 14

            email_template = SnotifierTemplate.objects.get(
                name="credit_letter"
            ).template

            if doc.docentry > int(sequence_einv_orin.value):
                sequence_einv_orin.value = str(doc.docentry)

        else:
            raise TypeError("UNKNOWN DOC TYPE")

        # find content
        try:
            b1_to_archive = (
                CksB1Toarchive.objects.filter(u_objtype=doc_obj_type)
                .filter(u_primarykey1=doc.docentry)
                .order_by("-docentry")
                .first()
            )
        except CksB1Toarchive.DoesNotExist:
            raise TypeError("Missing Docs in Archive, processing failed.")

        archive_ref = CksArchive.objects.get(docentry=b1_to_archive.u_archiveid)

        archive_file = CksFiles.objects.get(docentry=archive_ref.u_fileid)

        attachment = archive_get_attachment(
            doc_prefix, doc.docnum, b64decode(archive_file.u_file)
        )

        ticket_title = doc_prefix + " " + str(doc.docnum) + " - Dolphin IT"
        ticket_text = django_engine.from_string(email_template).render(template_opts)

        if doc.cardcode.u_dol_einv_status == 0 and doc.cardcode.u_dol_einv_email:
            if round(doc.doctotal, 2) <= 0:
                mail_to = "dev-null@ext.dolphin-it.de"
            else:
                mail_to = doc.cardcode.u_dol_einv_email

            contact = SnotifierEmailContactZammad(
                email=mail_to,
            )

            ticket_id = send(
                notifier_list=[contact],
                subject=ticket_title,
                text=ticket_text,
                group="Accounting",
                note=None,
                ticket_id=None,
                main_address=mail_to,
                attachments=[attachment],
                closed=True,
            )

            if isinstance(ticket_id, int):
                doc.u_dol_inv_sent_ticket = ticket_id
                doc.u_dol_inv_sent_date = datetime.now()
                doc.u_dol_inv_sent_status = 1
                doc.save()
            else:
                raise TypeError("Sending failed.")
        else:
            doc.u_dol_inv_sent_status = 2
            doc.save()

        sequence_einv_oinv.save()
        sequence_einv_orin.save()

    return


@shared_task(name="einvoice_tax_transfer")
def einvoice_tax_transfer():
    sequence_tax_transfer_oinv = InvoiceSettings.objects.get(
        name="sequence_tax_transfer_oinv"
    )
    sequence_tax_transfer_orin = InvoiceSettings.objects.get(
        name="sequence_tax_transfer_orin"
    )
    sequence_tax_transfer_dest_email = InvoiceSettings.objects.get(
        name="sequence_tax_transfer_dest_email"
    ).value
    sequence_tax_transfer_src_email = InvoiceSettings.objects.get(
        name="sequence_tax_transfer_src_email"
    ).value

    oinv_unprocessed = Oinv.objects.filter(
        docentry__gt=sequence_tax_transfer_oinv.value
    ).order_by("docentry")
    orin_unprocessed = Orin.objects.filter(
        docentry__gt=sequence_tax_transfer_orin.value
    ).order_by("docentry")

    document_list = list(chain(oinv_unprocessed, orin_unprocessed))

    for doc in document_list:
        doc_prefix = str()
        doc_obj_type = 0

        if isinstance(doc, Oinv):
            doc_prefix = "Rechnung"
            doc_obj_type = 13

            if doc.docentry > int(sequence_tax_transfer_oinv.value):
                sequence_tax_transfer_oinv.value = str(doc.docentry)

        elif isinstance(doc, Orin):
            doc_prefix = "Gutschrift"
            doc_obj_type = 14

            if doc.docentry > int(sequence_tax_transfer_orin.value):
                sequence_tax_transfer_orin.value = str(doc.docentry)

        else:
            raise TypeError("UNKNOWN DOC TYPE")

        # find content
        try:
            b1_to_archive = (
                CksB1Toarchive.objects.filter(u_objtype=doc_obj_type)
                .filter(u_primarykey1=doc.docentry)
                .order_by("-docentry")
                .first()
            )
        except CksB1Toarchive.DoesNotExist:
            raise TypeError("Missing Docs in Archive, processing failed.")

        archive_ref = CksArchive.objects.get(docentry=b1_to_archive.u_archiveid)

        archive_file = CksFiles.objects.get(docentry=archive_ref.u_fileid)

        email = EmailMessage(
            doc_prefix + " " + str(doc.docnum),
            doc_prefix + " " + str(doc.docnum),
            sequence_tax_transfer_src_email,
            [sequence_tax_transfer_dest_email],
            reply_to=[sequence_tax_transfer_src_email],
        )

        email.attach(
            doc_prefix + "_" + str(doc.docnum) + ".pdf",
            b64decode(archive_file.u_file),
            "application/pdf",
        )

        if email.send():
            sequence_tax_transfer_oinv.save()
            sequence_tax_transfer_orin.save()
        else:
            raise TypeError("Sending email failed.")

    return


@shared_task(name="einvoice_factoring_transfer")
def einvoice_factoring_transfer():
    care_api = slumber.API(
        settings.ZAMMAD_URL, auth=(settings.ZAMMAD_USER, settings.ZAMMAD_PASSWD)
    )

    factoring_export_summary = BytesIO(
        InvoiceSettingsData.objects.get(name="factoring_export_summary").value
    )

    factoring_seq_obj = InvoiceSettings.objects.get(name="sequence_factoring_num")
    factoring_seq = int(factoring_seq_obj.value) + 1

    split_archive_count = InvoiceSettings.objects.get(
        name="factoring_split_doc_archive"
    ).value
    split_archive_count = int(split_archive_count)

    factoring_custnr = InvoiceSettings.objects.get(name="factoring_cust_nr").value
    factoring_dest_email = InvoiceSettings.objects.get(
        name="factoring_dest_email"
    ).value
    factoring_int_cust_nr = int(
        InvoiceSettings.objects.get(name="factoring_int_cust_nr").value
    )

    tempdirpath = tempfile.mkdtemp()

    print(tempdirpath)

    filename_export = "factoring_export_seq_" + str(factoring_seq)

    workbook = xlsxwriter.Workbook(os.path.join(tempdirpath, filename_export + ".xlsx"))
    worksheet = workbook.add_worksheet(name="factoring_export")

    # Ihre Kundennummer bei uns, max length (20), nachkommastellen ()
    worksheet.write("A1", "Ihre Kundennummer bei uns")

    # Debitorennummer, max length (20), nachkommastellen ()
    worksheet.write("B1", "Debitorennummer")

    # Debitorenname 1, max length (50), nachkommastellen ()
    worksheet.write("C1", "Debitorenname 1")

    # Debitorenname 2, max length (50), nachkommastellen ()
    worksheet.write("D1", "Debitorenname 2")

    # Strasse (hier auch mit Haus-Nr möglich), max length (50), nachkommastellen ()
    worksheet.write("E1", "Strasse (hier auch mit Haus-Nr möglich)")

    # Haus-Nr., max length (10), nachkommastellen ()
    worksheet.write("F1", "Haus-Nr.")

    # Postfach-Nr (falls vorhanden), max length (10), nachkommastellen ()
    worksheet.write("G1", "Postfach-Nr (falls vorhanden)")

    # PLZ, max length (15), nachkommastellen ()
    worksheet.write("H1", "PLZ")

    # Ort, max length (50), nachkommastellen ()
    worksheet.write("I1", "Ort")

    # Nationenkennzeichen (alternativ: Nation ausgeschrieben), max length (5), nachkommastellen ()
    worksheet.write("J1", "Nationenkennzeichen (alternativ: Nation ausgeschrieben)")

    # -, max length (2000), nachkommastellen ()
    worksheet.write("K1", "-")

    # -, max length (200), nachkommastellen ()
    worksheet.write("L1", "-")

    # Telefonummer 1, max length (30), nachkommastellen ()
    worksheet.write("M1", "Telefonummer 1")

    # Telefonummer 2, max length (30), nachkommastellen ()
    worksheet.write("N1", "Telefonummer 2")

    # Mobilnummer, max length (30), nachkommastellen ()
    worksheet.write("O1", "Mobilnummer")

    # Faxnummer, max length (30), nachkommastellen ()
    worksheet.write("P1", "Faxnummer")

    # Emailadresse, max length (100), nachkommastellen ()
    worksheet.write("Q1", "Emailadresse")

    # Homepage, max length (100), nachkommastellen ()
    worksheet.write("R1", "Homepage")

    # Handelsregisternummer, max length (25), nachkommastellen ()
    worksheet.write("S1", "Handelsregisternummer")

    # Handelsregisterort, max length (50), nachkommastellen ()
    worksheet.write("T1", "Handelsregisterort")

    # -, max length (20), nachkommastellen ()
    worksheet.write("U1", "-")

    # -, max length (1), nachkommastellen (0)
    worksheet.write("V1", "-")

    # sonstige Info zum Debitor (Freitext), max length (2000), nachkommastellen ()
    worksheet.write("W1", "sonstige Info zum Debitor (Freitext)")

    # Belegart (RE, Rechnung, GS, Gutschrift), max length (5), nachkommastellen ()
    worksheet.write("X1", "Belegart (RE, Rechnung, GS, Gutschrift)")

    # -, max length (3), nachkommastellen ()
    worksheet.write("Y1", "-")

    # Rechnung-/Gutschriftnummer, max length (20), nachkommastellen ()
    worksheet.write("Z1", "Rechnung-/Gutschriftnummer")

    # Bruttobetrag, max length (15), nachkommastellen (2)
    worksheet.write("AA1", "Bruttobetrag")

    # Rechnung-/Gutschriftdatum, max length (dd.mm.yyyy), nachkommastellen ()
    worksheet.write("AB1", "Rechnung-/Gutschriftdatum")

    # -, max length (2), nachkommastellen ()
    worksheet.write("AC1", "-")

    # USt 1, max length (20), nachkommastellen ()
    worksheet.write("AD1", "USt 1")

    # Bruttobetrag auf Ust 1, max length (15), nachkommastellen (2)
    worksheet.write("AE1", "Bruttobetrag auf Ust 1")

    # USt 2, max length (20), nachkommastellen ()
    worksheet.write("AF1", "USt 2")

    # Bruttobetrag auf Ust 2, max length (15), nachkommastellen (2)
    worksheet.write("AG1", "Bruttobetrag auf Ust 2")

    # USt 3, max length (20), nachkommastellen ()
    worksheet.write("AH1", "USt 3")

    # Bruttobetrag auf Ust 3, max length (15), nachkommastellen (2)
    worksheet.write("AI1", "Bruttobetrag auf Ust 3")

    # Fällig am, max length (dd.mm.yyyy), nachkommastellen ()
    worksheet.write("AJ1", "Fällig am")

    # Skontosatz 1, max length (5), nachkommastellen (2)
    worksheet.write("AK1", "Skontosatz 1")

    # Skontotage 1, max length (11), nachkommastellen (0)
    worksheet.write("AL1", "Skontotage 1")

    # Skontosatz 2, max length (5), nachkommastellen (2)
    worksheet.write("AM1", "Skontosatz 2")

    # Skontotage 2, max length (11), nachkommastellen (0)
    worksheet.write("AN1", "Skontotage 2")

    # Skontosatz 3, max length (5), nachkommastellen (2)
    worksheet.write("AO1", "Skontosatz 3")

    # Skontotage 3, max length (11), nachkommastellen (0)
    worksheet.write("AP1", "Skontotage 3")

    # Skontosatz 4, max length (5), nachkommastellen (2)
    worksheet.write("AQ1", "Skontosatz 4")

    # Skontotage 4, max length (11), nachkommastellen (0)
    worksheet.write("AR1", "Skontotage 4")

    # Skontosatz 5, max length (5), nachkommastellen (2)
    worksheet.write("AS1", "Skontosatz 5")

    # Skontotage 5, max length (11), nachkommastellen (0)
    worksheet.write("AT1", "Skontotage 5")

    # Nettotage, max length (11), nachkommastellen (0)
    worksheet.write("AU1", "Nettotage")

    # -, max length (5), nachkommastellen ()
    worksheet.write("AV1", "-")

    # -, max length (11), nachkommastellen (0)
    worksheet.write("AW1", "-")

    # sonstige Info zum Beleg (Freitext), max length (2000), nachkommastellen ()
    worksheet.write("AX1", "sonstige Info zum Beleg (Freitext)")

    # -, max length (2000), nachkommastellen ()
    worksheet.write("AY1", "-")

    # -, max length (11), nachkommastellen (0)
    worksheet.write("AZ1", "-")

    # -, max length (), nachkommastellen ()
    worksheet.write("BA1", "-")

    # Nettobetrag zu USt 1, max length (15), nachkommastellen (2)
    worksheet.write("BB1", "Nettobetrag zu USt 1")

    # USt-Betrag 1, max length (15), nachkommastellen (2)
    worksheet.write("BC1", "USt-Betrag 1")

    # Nettobetrag zu USt 2, max length (15), nachkommastellen (2)
    worksheet.write("BD1", "Nettobetrag zu USt 2")

    # USt-Betrag 2, max length (15), nachkommastellen (2)
    worksheet.write("BE1", "USt-Betrag 2")

    # Nettobetrag zu USt 3, max length (15), nachkommastellen (2)
    worksheet.write("BF1", "Nettobetrag zu USt 3")

    # USt-Betrag 3, max length (15), nachkommastellen (2)
    worksheet.write("BG1", "USt-Betrag 3")

    # -, max length (5), nachkommastellen ()
    worksheet.write("BH1", "-")

    # -, max length (5), nachkommastellen ()
    worksheet.write("BI1", "-")

    # -, max length (5), nachkommastellen ()
    worksheet.write("BJ1", "-")

    # -, max length (20), nachkommastellen ()
    worksheet.write("BK1", "-")

    # -, max length (50), nachkommastellen ()
    worksheet.write("BL1", "-")

    # -, max length (1), nachkommastellen (0)
    worksheet.write("BM1", "-")

    # Zahlungsbedingung als Text (z.B.10 Tage 3%, 30 Tage netto // möglich bei fixem Katalog welcher Crefo-Factoring vorab mitgeteilt wird), max length (20), nachkommastellen ()
    worksheet.write(
        "BN1",
        "Zahlungsbedingung als Text (z.B.10 Tage 3%, 30 Tage netto // möglich bei fixem Katalog welcher Crefo-Factoring vorab mitgeteilt wird)",
    )

    # Geburtstdatum (bei Privatperson), max length (dd.mm.yyyy), nachkommastellen ()
    worksheet.write("BO1", "Geburtstdatum (bei Privatperson)")

    # Referenznummer (Lieferscheinnummer, Bestellnummer etc.), max length (20), nachkommastellen ()
    worksheet.write("BP1", "Referenznummer (Lieferscheinnummer, Bestellnummer etc.)")

    # -, max length (12), nachkommastellen ()
    worksheet.write("BQ1", "-")

    # -, max length (20), nachkommastellen ()
    worksheet.write("BR1", "-")

    # -, max length (50), nachkommastellen ()
    worksheet.write("BS1", "-")

    # -, max length (50), nachkommastellen ()
    worksheet.write("BT1", "-")

    # -, max length (50), nachkommastellen ()
    worksheet.write("BU1", "-")

    # -, max length (20), nachkommastellen ()
    worksheet.write("BV1", "-")

    # Steuernummer, max length (25), nachkommastellen ()
    worksheet.write("BW1", "Steuernummer")

    # Umsatzsteuer-ID, max length (25), nachkommastellen ()
    worksheet.write("BX1", "Umsatzsteuer-ID")

    # Sperre Ankauf, max length (1), nachkommastellen (0)
    worksheet.write("BY1", "Sperre Ankauf")

    # Geschäftstyp (B2B / B2C), max length (3), nachkommastellen ()
    worksheet.write("BZ1", "Geschäftstyp (B2B / B2C)")

    # -, max length (1), nachkommastellen (0)
    worksheet.write("CA1", "-")

    # -, max length (), nachkommastellen ()
    worksheet.write("CB1", "-")

    # ja, nein
    worksheet.write("CC1", "Versandweg")

    # Datetime
    worksheet.write("CD1", "Versandzeitpunkt falls bereits erfolgt")

    seq_einv_oinv = InvoiceSettings.objects.get(name="sequence_einv_oinv")
    seq_einv_orin = InvoiceSettings.objects.get(name="sequence_einv_orin")

    seq_factoring_oinv = InvoiceSettings.objects.get(name="sequence_factoring_oinv")
    seq_factoring_orin = InvoiceSettings.objects.get(name="sequence_factoring_orin")

    factoring_list_inv = (
        Oinv.objects.filter(docentry__gt=int(seq_factoring_oinv.value))
        .filter(docentry__lte=int(seq_einv_oinv.value))
        .exclude(cardcode=settings.MDAT_ROOT_CUSTOMER_ID)
        .exclude(doctotal=0)
        .exclude(u_dol_inv_factoring=-1)
        .order_by("docentry")
    )

    factoring_list_credit = (
        Orin.objects.filter(docentry__gt=int(seq_factoring_orin.value))
        .filter(docentry__lte=int(seq_einv_orin.value))
        .exclude(cardcode=settings.MDAT_ROOT_CUSTOMER_ID)
        .exclude(doctotal=0)
        .exclude(u_dol_inv_factoring=-1)
        .order_by("docentry")
    )

    factoring_list = list(chain(factoring_list_inv, factoring_list_credit))

    factoring_row_count = 2

    total_sum = dict()
    total_sum["13"] = 0
    total_sum["14"] = 0

    # file count
    i = 0
    c = 0

    # stop if no documents
    if len(factoring_list) <= 0:
        print("no invoices or credit letters")
        return

    for factoring_entry in factoring_list:
        doc_obj_type = 0
        doc_prefix = str()

        customer = MdatCustomers.objects.get(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
            external_id=factoring_entry.cardcode.cardcode,
        )

        i += 1

        if i > split_archive_count:
            i = 1
            c += 1

        split_directory = os.path.join(tempdirpath, str(c))

        try:
            os.mkdir(split_directory)
        except OSError:
            pass

        if isinstance(factoring_entry, Oinv):
            doc_obj_type = 13
            doc_prefix = "Rechnung"

            if int(seq_factoring_oinv.value) < factoring_entry.docentry:
                seq_factoring_oinv.value = str(factoring_entry.docentry)
        elif isinstance(factoring_entry, Orin):
            doc_obj_type = 14
            doc_prefix = "Gutschrift"

            if int(seq_factoring_orin.value) < factoring_entry.docentry:
                seq_factoring_orin.value = str(factoring_entry.docentry)
        else:
            raise TypeError("Unknown Doctype!")

        total_sum[str(doc_obj_type)] += round(factoring_entry.doctotal, 2)

        # find content
        try:
            b1_to_archive = (
                CksB1Toarchive.objects.filter(u_objtype=doc_obj_type)
                .filter(u_primarykey1=factoring_entry.docentry)
                .order_by("-docentry")
                .first()
            )
        except CksB1Toarchive.DoesNotExist:
            raise TypeError("Missing Docs in Archive, processing failed.")

        archive_ref = CksArchive.objects.get(docentry=b1_to_archive.u_archiveid)

        archive_file = CksFiles.objects.get(docentry=archive_ref.u_fileid)

        filename = os.path.join(
            split_directory,
            "DolphinIT_" + doc_prefix + "_" + str(factoring_entry.docnum) + ".pdf",
        )
        pdf_file = open(filename, "wb")
        pdf_file.write(b64decode(archive_file.u_file))
        pdf_file.close()

        # Ihre Kundennummer bei uns, max length (20), nachkommastellen ()
        worksheet.write("A" + str(factoring_row_count), factoring_custnr)

        # Debitorennummer, max length (20), nachkommastellen ()
        worksheet.write(
            "B" + str(factoring_row_count), factoring_entry.cardcode.cardcode
        )

        # Debitorenname 1, max length (50), nachkommastellen ()
        worksheet.write(
            "C" + str(factoring_row_count), factoring_entry.cardcode.cardname[0:49]
        )

        # Debitorenname 2, max length (50), nachkommastellen ()
        worksheet.write(
            "D" + str(factoring_row_count), factoring_entry.cardcode.cardname[49:99]
        )

        # Strasse (hier auch mit Haus-Nr möglich), max length (50), nachkommastellen ()
        worksheet.write(
            "E" + str(factoring_row_count),
            customer.mdatcustomerbillingaddresses.address.street.title,
        )

        # Haus-Nr., max length (10), nachkommastellen ()
        worksheet.write(
            "F" + str(factoring_row_count),
            customer.mdatcustomerbillingaddresses.address.house_nr,
        )

        # Postfach-Nr (falls vorhanden), max length (10), nachkommastellen ()
        worksheet.write("G" + str(factoring_row_count), "")

        # PLZ, max length (15), nachkommastellen ()
        worksheet.write(
            "H" + str(factoring_row_count),
            customer.mdatcustomerbillingaddresses.address.street.city.zip_code,
        )

        # Ort, max length (50), nachkommastellen ()
        worksheet.write(
            "I" + str(factoring_row_count),
            customer.mdatcustomerbillingaddresses.address.street.city.title,
        )

        # Nationenkennzeichen (alternativ: Nation ausgeschrieben), max length (5), nachkommastellen ()
        worksheet.write(
            "J" + str(factoring_row_count),
            customer.mdatcustomerbillingaddresses.address.street.city.country.title,
        )

        # -, max length (2000), nachkommastellen ()
        worksheet.write("K" + str(factoring_row_count), "")

        # -, max length (200), nachkommastellen ()
        worksheet.write("L" + str(factoring_row_count), "")

        # Telefonummer 1, max length (30), nachkommastellen ()
        worksheet.write("M" + str(factoring_row_count), factoring_entry.cardcode.phone1)

        # Telefonummer 2, max length (30), nachkommastellen ()
        worksheet.write("N" + str(factoring_row_count), factoring_entry.cardcode.phone2)

        # Mobilnummer, max length (30), nachkommastellen ()
        worksheet.write(
            "O" + str(factoring_row_count), factoring_entry.cardcode.cellular
        )

        # Faxnummer, max length (30), nachkommastellen ()
        worksheet.write("P" + str(factoring_row_count), factoring_entry.cardcode.fax)

        # Emailadresse, max length (100), nachkommastellen ()
        worksheet.write("Q" + str(factoring_row_count), factoring_entry.cardcode.e_mail)

        # Homepage, max length (100), nachkommastellen ()
        worksheet.write(
            "R" + str(factoring_row_count), factoring_entry.cardcode.intrntsite
        )

        # Handelsregisternummer, max length (25), nachkommastellen ()
        worksheet.write("S" + str(factoring_row_count), "")

        # Handelsregisterort, max length (50), nachkommastellen ()
        worksheet.write("T" + str(factoring_row_count), "")

        # -, max length (20), nachkommastellen ()
        worksheet.write("U" + str(factoring_row_count), "")

        # -, max length (1), nachkommastellen (0)
        worksheet.write("V" + str(factoring_row_count), "")

        # sonstige Info zum Debitor (Freitext), max length (2000), nachkommastellen ()
        worksheet.write("W" + str(factoring_row_count), "")

        if isinstance(factoring_entry, Oinv):
            # Belegart (RE, Rechnung, GS, Gutschrift), max length (5), nachkommastellen ()
            worksheet.write("X" + str(factoring_row_count), "RE")
        elif isinstance(factoring_entry, Orin):
            # Belegart (RE, Rechnung, GS, Gutschrift), max length (5), nachkommastellen ()
            worksheet.write("X" + str(factoring_row_count), "GS")
        else:
            raise TypeError("Unknown Doc type!")

        # -, max length (3), nachkommastellen ()
        worksheet.write("Y" + str(factoring_row_count), "")

        # Rechnung-/Gutschriftnummer, max length (20), nachkommastellen ()
        worksheet.write("Z" + str(factoring_row_count), factoring_entry.docnum)

        # Bruttobetrag, max length (15), nachkommastellen (2)
        worksheet.write(
            "AA" + str(factoring_row_count),
            str(round(factoring_entry.doctotal, 2)).replace(".", ","),
        )

        # Rechnung-/Gutschriftdatum, max length (dd.mm.yyyy), nachkommastellen ()
        worksheet.write(
            "AB" + str(factoring_row_count),
            factoring_entry.docdate.strftime("%d.%m.%Y"),
        )

        # -, max length (2), nachkommastellen ()
        worksheet.write("AC" + str(factoring_row_count), "")

        # USt 1, max length (20), nachkommastellen ()
        worksheet.write("AD" + str(factoring_row_count), "")

        # Bruttobetrag auf Ust 1, max length (15), nachkommastellen (2)
        worksheet.write("AE" + str(factoring_row_count), "")

        # USt 2, max length (20), nachkommastellen ()
        worksheet.write("AF" + str(factoring_row_count), "")

        # Bruttobetrag auf Ust 2, max length (15), nachkommastellen (2)
        worksheet.write("AG" + str(factoring_row_count), "")

        # USt 3, max length (20), nachkommastellen ()
        worksheet.write("AH" + str(factoring_row_count), "")

        # Bruttobetrag auf Ust 3, max length (15), nachkommastellen (2)
        worksheet.write("AI" + str(factoring_row_count), "")

        # Fällig am, max length (dd.mm.yyyy), nachkommastellen ()
        worksheet.write(
            "AJ" + str(factoring_row_count),
            factoring_entry.docduedate.strftime("%d.%m.%Y"),
        )

        # Skontosatz 1, max length (5), nachkommastellen (2)
        worksheet.write("AK" + str(factoring_row_count), "")

        # Skontotage 1, max length (11), nachkommastellen (0)
        worksheet.write("AL" + str(factoring_row_count), "")

        # Skontosatz 2, max length (5), nachkommastellen (2)
        worksheet.write("AM" + str(factoring_row_count), "")

        # Skontotage 2, max length (11), nachkommastellen (0)
        worksheet.write("AN" + str(factoring_row_count), "")

        # Skontosatz 3, max length (5), nachkommastellen (2)
        worksheet.write("AO" + str(factoring_row_count), "")

        # Skontotage 3, max length (11), nachkommastellen (0)
        worksheet.write("AP" + str(factoring_row_count), "")

        # Skontosatz 4, max length (5), nachkommastellen (2)
        worksheet.write("AQ" + str(factoring_row_count), "")

        # Skontotage 4, max length (11), nachkommastellen (0)
        worksheet.write("AR" + str(factoring_row_count), "")

        # Skontosatz 5, max length (5), nachkommastellen (2)
        worksheet.write("AS" + str(factoring_row_count), "")

        # Skontotage 5, max length (11), nachkommastellen (0)
        worksheet.write("AT" + str(factoring_row_count), "")

        # Nettotage, max length (11), nachkommastellen (0)
        worksheet.write("AU" + str(factoring_row_count), "")

        # -, max length (5), nachkommastellen ()
        worksheet.write("AV" + str(factoring_row_count), "")

        # -, max length (11), nachkommastellen (0)
        worksheet.write("AW" + str(factoring_row_count), "")

        # sonstige Info zum Beleg (Freitext), max length (2000), nachkommastellen ()
        worksheet.write("AX" + str(factoring_row_count), factoring_entry.comments)

        # -, max length (2000), nachkommastellen ()
        worksheet.write("AY" + str(factoring_row_count), "")

        # -, max length (11), nachkommastellen (0)
        worksheet.write("AZ" + str(factoring_row_count), "")

        # -, max length (), nachkommastellen ()
        worksheet.write("BA" + str(factoring_row_count), "")

        # Nettobetrag zu USt 1, max length (15), nachkommastellen (2)
        worksheet.write("BB" + str(factoring_row_count), "")

        # USt-Betrag 1, max length (15), nachkommastellen (2)
        worksheet.write("BC" + str(factoring_row_count), "")

        # Nettobetrag zu USt 2, max length (15), nachkommastellen (2)
        worksheet.write("BD" + str(factoring_row_count), "")

        # USt-Betrag 2, max length (15), nachkommastellen (2)
        worksheet.write("BE" + str(factoring_row_count), "")

        # Nettobetrag zu USt 3, max length (15), nachkommastellen (2)
        worksheet.write("BF" + str(factoring_row_count), "")

        # USt-Betrag 3, max length (15), nachkommastellen (2)
        worksheet.write("BG" + str(factoring_row_count), "")

        # -, max length (5), nachkommastellen ()
        worksheet.write("BH" + str(factoring_row_count), "")

        # -, max length (5), nachkommastellen ()
        worksheet.write("BI" + str(factoring_row_count), "")

        # -, max length (5), nachkommastellen ()
        worksheet.write("BJ" + str(factoring_row_count), "")

        # -, max length (20), nachkommastellen ()
        worksheet.write("BK" + str(factoring_row_count), "")

        # -, max length (50), nachkommastellen ()
        worksheet.write("BL" + str(factoring_row_count), "")

        # -, max length (1), nachkommastellen (0)
        worksheet.write("BM" + str(factoring_row_count), "")

        # Zahlungsbedingung als Text (z.B.10 Tage 3%, 30 Tage netto // möglich bei fixem Katalog welcher Crefo-Factoring vorab mitgeteilt wird), max length (20), nachkommastellen ()
        payment_terms = Octg.objects.get(groupnum=factoring_entry.groupnum)
        worksheet.write("BN" + str(factoring_row_count), payment_terms.pymntgroup)

        # Geburtstdatum (bei Privatperson), max length (dd.mm.yyyy), nachkommastellen ()
        worksheet.write("BO" + str(factoring_row_count), "")

        # Referenznummer (Lieferscheinnummer, Bestellnummer etc.), max length (20), nachkommastellen ()
        worksheet.write("BP" + str(factoring_row_count), factoring_entry.numatcard)

        # -, max length (12), nachkommastellen ()
        worksheet.write("BQ" + str(factoring_row_count), "")

        # -, max length (20), nachkommastellen ()
        worksheet.write("BR" + str(factoring_row_count), "")

        # -, max length (50), nachkommastellen ()
        worksheet.write("BS" + str(factoring_row_count), "")

        # -, max length (50), nachkommastellen ()
        worksheet.write("BT" + str(factoring_row_count), "")

        # -, max length (50), nachkommastellen ()
        worksheet.write("BU" + str(factoring_row_count), "")

        # -, max length (20), nachkommastellen ()
        worksheet.write("BV" + str(factoring_row_count), "")

        # Steuernummer, max length (25), nachkommastellen ()
        worksheet.write("BW" + str(factoring_row_count), "")

        # Umsatzsteuer-ID, max length (25), nachkommastellen ()
        worksheet.write("BX" + str(factoring_row_count), "")

        # Sperre Ankauf, max length (1), nachkommastellen (0)
        worksheet.write("BY" + str(factoring_row_count), "")

        # Geschäftstyp (B2B / B2C), max length (3), nachkommastellen ()
        worksheet.write("BZ" + str(factoring_row_count), "")

        # -, max length (1), nachkommastellen (0)
        worksheet.write("CA" + str(factoring_row_count), "")

        # -, max length (), nachkommastellen ()
        worksheet.write("CB" + str(factoring_row_count), "")

        if factoring_entry.u_dol_inv_sent_status == "0":
            raise TypeError("Unprocessed Data!")
        elif factoring_entry.u_dol_inv_sent_status == "1":
            # einv email
            worksheet.write(
                "CC" + str(factoring_row_count), "E-Rechnung Dolphin"
            )  # einv ja / nein
            worksheet.write(
                "CD" + str(factoring_row_count),
                factoring_entry.u_dol_inv_sent_date.strftime("%d.%m.%Y %H:%M"),
            )  # einv datum
        elif factoring_entry.u_dol_inv_sent_status == "2":
            # no einv
            worksheet.write("CC" + str(factoring_row_count), "Post")  # einv ja / nein
        else:
            raise TypeError("Unprocessed Data!")

        factoring_row_count = factoring_row_count + 1

    workbook.close()

    packet = BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=A4)
    can.setFont("Helvetica", 18)

    # factoring sequence
    can.drawString(2.90 * cm, 15.60 * cm, str(factoring_seq))

    # invoice count
    can.drawString(7.90 * cm, 15.60 * cm, str(len(factoring_list_inv)) + " Stück")

    # credit letter count
    can.drawString(7.90 * cm, 14.10 * cm, str(len(factoring_list_credit)) + " Stück")

    # invoice sum
    can.drawString(13.90 * cm, 15.60 * cm, str(total_sum["13"]).replace(".", ","))

    # credit letter sum
    can.drawString(13.90 * cm, 14.10 * cm, str(total_sum["14"]).replace(".", ","))

    # city and date
    can.setFont("Helvetica", 16)
    can.drawString(2.50 * cm, 4.10 * cm, str("Hückeswagen,"))
    can.drawString(2.50 * cm, 3.50 * cm, str(datetime.now().strftime("%d.%m.%Y %H:%M")))

    can.save()

    # move to the beginning of the StringIO buffer
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    # read your existing PDF
    existing_pdf = PdfFileReader(factoring_export_summary)
    output = PdfFileWriter()
    # add the "watermark" (which is the new pdf) on the existing page
    page = existing_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    output.addPage(page)
    # finally, write "output" to a real file
    outputStream = open(os.path.join(tempdirpath, filename_export + ".pdf"), "wb")
    output.write(outputStream)
    outputStream.close()

    # Prepare ticket
    django_engine = engines["django"]

    txt_ticketmail = SnotifierTemplate.objects.get(name="factoring").template

    ticket_title = "Rechnungseinreichung Nr. " + str(factoring_seq)

    email_counter = 0

    # create new ticket for sequence
    email_sum = ceil(len(factoring_list) / split_archive_count) + 1
    factoring_customer = Ocrd.objects.get(cardcode=factoring_int_cust_nr)

    template_opts = dict()
    template_opts["customer_code"] = factoring_customer.cardcode
    template_opts["customer_name"] = factoring_customer.cardname
    template_opts["customer_email"] = factoring_customer.e_mail
    template_opts["sequence_factoring_num"] = str(factoring_seq)
    template_opts["inv_count"] = str(len(factoring_list_inv)) + " Stück"
    template_opts["inv_sum"] = str(total_sum["13"]).replace(".", ",") + " Euro"
    template_opts["credit_count"] = str(len(factoring_list_credit)) + " Stück"
    template_opts["credit_sum"] = str(total_sum["14"]).replace(".", ",") + " Euro"
    template_opts["max_files"] = str(split_archive_count)

    ticketdata = dict()
    ticketdata["title"] = ticket_title
    ticketdata["group"] = "Accounting"
    ticketdata["state_id"] = 4
    ticketdata["owner_id"] = 6
    ticketdata["priority_id"] = 3
    ticketdata["customer_id"] = "guess:" + factoring_dest_email
    ticketdata["article"] = dict()
    ticketdata["article"]["subject"] = ticket_title
    ticketdata["article"]["body"] = ticket_title
    ticketdata["article"]["type"] = "note"
    ticketdata["article"]["internal"] = False
    ticketdata["note"] = ticket_title

    ticket = care_api.tickets.post(ticketdata)
    # id is saved in: ticket['id']

    # send first email with excel and pdf export
    template_ticketmail = django_engine.from_string(txt_ticketmail)

    email_counter += 1

    articledata = dict()
    articledata["ticket_id"] = ticket["id"]
    articledata["to"] = factoring_dest_email
    articledata["subject"] = (
        ticket_title + " [Teil " + str(email_counter) + "/" + str(email_sum) + "]"
    )
    articledata["body"] = template_ticketmail.render(template_opts)
    articledata["content_type"] = "text/plain"
    articledata["type"] = "email"
    articledata["internal"] = False

    ticket_attachments = list()

    for filetype in [".pdf", ".xlsx"]:
        attachment = dict()
        attachment["filename"] = filename_export + filetype
        local_file = open(
            os.path.join(tempdirpath, filename_export + filetype), "rb"
        ).read()
        attachment["data"] = b64encode(local_file).decode("utf-8")
        if filetype == ".pdf":
            attachment["mime-type"] = "application/pdf"
        elif filetype == ".xlsx":
            attachment["mime-type"] = (
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            )
        else:
            raise TypeError("Unknown Filetype")

        ticket_attachments.append(attachment)

    articledata["attachments"] = ticket_attachments

    article = care_api.ticket_articles.post(articledata)

    # send n emails with zip collection(s)
    #         ticket_attachments.append(archive_get_attachment(prefix, document.docnum, archive_file.image))

    for inv_mail_count in range(c + 1):
        email_counter += 1

        articledata = dict()
        articledata["ticket_id"] = ticket["id"]
        articledata["to"] = factoring_dest_email
        articledata["subject"] = (
            ticket_title + " [Teil " + str(email_counter) + "/" + str(email_sum) + "]"
        )
        articledata["body"] = template_ticketmail.render(template_opts)
        articledata["content_type"] = "text/plain"
        articledata["type"] = "email"
        articledata["internal"] = False

        ticket_attachments = list()

        export_zip_name = filename_export + "_part" + str(email_counter - 1).zfill(4)

        shutil.make_archive(
            os.path.join(tempdirpath, export_zip_name),
            "zip",
            os.path.join(tempdirpath, str(inv_mail_count)),
        )

        attachment = dict()
        attachment["filename"] = export_zip_name + ".zip"
        local_file = open(
            os.path.join(tempdirpath, export_zip_name + ".zip"), "rb"
        ).read()
        attachment["data"] = b64encode(local_file).decode("utf-8")
        attachment["mime-type"] = "application/zip"

        ticket_attachments.append(attachment)

        articledata["attachments"] = ticket_attachments

        article = care_api.ticket_articles.post(articledata)

    for factoring_entry in factoring_list:
        factoring_entry.u_dol_inv_factoring = 1
        factoring_entry.save()

    factoring_seq_obj.value = str(factoring_seq)
    factoring_seq_obj.save()

    # save incremented docentry
    seq_factoring_oinv.save()
    seq_factoring_orin.save()

    # delete whole tmp folder after run
    shutil.rmtree(tempdirpath)

    return
